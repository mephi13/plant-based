-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Czas generowania: 13 Gru 2021, 11:15
-- Wersja serwera: 8.0.27
-- Wersja PHP: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `plantbased`
--
CREATE DATABASE IF NOT EXISTS `plantbased` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `plantbased`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `diseases`
--

CREATE TABLE IF NOT EXISTS `diseases` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `treatment` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Zrzut danych tabeli `diseases`
--

INSERT INTO `diseases` (`id`, `name`, `description`, `treatment`) VALUES
(1, 'Poisonous', 'Toxic for house animals and for children. ', 'Do not eat it! For real!'),
(2, 'Test', 'Test', 'advadv'),
(7, 'Anthracnose', 'The fungi Colletrotrichum and Gloeosporium cause anthracnose. The leaf tips turn yellow, then tan, then dark brown. The browning may extend completely around the leaf. The leaves eventually die. Wounding enhances penetration by these fungi.', 'Pick off and destroy infected leaves. Do not mist leaves. Sprays of copper soap, chlorothalonil, myclobutanil, or tebuconazole can be used after removing infected plant parts in order to reduce the incidence of future disease'),
(8, 'Fungal Leaf Spots', 'Several fungi can cause leaf spots. Symptoms include small, brown spots with yellowish margins on the leaves. Spots may have a concentric ring or target pattern. Small black dots (fruiting bodies of the fungi) may be visible in dead tissue. Sometimes the lesions run together, and the entire leaf dies. The fungi survive on dead and decaying plant matter in the soil.', 'Remove and destroy infected plant material. Provide good air circulation around the plants. Avoid splashing water on the foliage since this spreads the fungi. Sprays of copper soap, chlorothalonil, myclobutanil, tebuconazole, or Bacillus amyloliquefaciens can be used after removing infected plant parts in order to reduce the incidence of future disease.'),
(9, 'Bacterial Leaf Spots', 'Plants infected with bacteria have water-soaked spots, sometimes with a yellow halo, usually uniform in size, and sometimes with a sticky ooze. The spots enlarge and run together during wet conditions, such as if plants are placed outside during rainy periods. Under drier conditions, the spots do not enlarge but dry out and turn reddish-brown, giving a speckled appearance.', 'Remove all diseased plant material. Avoid low temperatures, crowding plants, and spraying or splashing water onto the foliage. Sprays of copper soap or Bacillus amyloliquefaciens can be used after removing infected plant parts in order to reduce the incidence of future disease.'),
(10, 'Aphids', 'Aphids are small, soft-bodied, pear-shaped insects about 1/16– to ⅛-inch long. They are usually green but may be pink, brown, black, or yellow. Some aphids have a woolly or powdery appearance because of a waxy coat. Adults may or may not have wings.', 'With minor infestations, handpicking, spraying with water, or wiping the insects with a cotton swab dipped in rubbing alcohol may be practical. Insecticidal soap spray may also be used. In most cases, the treatment will have to be repeated multiple times. For houseplants that are taken outdoors, spray with insecticidal soap, neem oil extract, pyrethrins, imidacloprid, cyfluthrin, deltamethrin, or lambda cyhalothrin to control aphids'),
(11, 'Mealybugs', 'Mealybugs are small, pale insects related to scales. They are about ⅛ to ¼ inch long and move very sluggishly. The adult females cover themselves and their eggs with a white, waxy material, making them look cottony. Some have waxy filaments that extend beyond their bodies.', 'Light infestations can be controlled by removing individual mealybugs by hand or by wiping each insect with a cotton swab dipped in rubbing alcohol. An insecticidal soap spray may also be used. With a heavy infestation, it may be necessary to discard the plant. For houseplants that are outdoors, spray with neem oil extract, pyrethrins, acetamiprid, imidacloprid, cyfluthrin, deltamethrin, or lambda cyhalothrin to control mealybugs. Imidacloprid granules put onto the soil will also control mealybugs'),
(12, 'Spider Mites', 'Mites are not insects but are more closely related to spiders. Since they are extremely small, plant damage is typically the first sign of their presence. A silky web is often seen with heavier infestations.', 'Spray sturdy plants forcefully with water, including the undersides of leaves, to dislodge mites and break up their webs. Plants also can be sprayed with insecticidal soap. For houseplants that are outdoors, spray with insecticidal soap, neem oil extract, or an insecticide containing sulfur. It is often necessary to spray once a week for several weeks to control mites'),
(13, 'Fungus Gnats', 'Adult fungus gnats are delicate in appearance and about 1/8-inch long. Often they can be seen running across or flying near the soil surface under a houseplant. They are weak flyers and are attracted to light.', 'For plants that can tolerate it (i.e., most houseplants, especially during winter), allow the soil to dry between watering. Dry conditions will kill the larvae. Do not allow water to stand in the saucer beneath houseplant containers, and invert saucers beneath plants outside to not collect rainwater. Products that contain strains of the biological control agent Bacillus thuringiensis subspecies israelensis can be applied to the soil of houseplants and watered into the soil for control');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `plants`
--

CREATE TABLE IF NOT EXISTS `plants` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `specie` varchar(255) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `watering_weekly` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Zrzut danych tabeli `plants`
--

INSERT INTO `plants` (`id`, `type`, `specie`, `description`, `watering_weekly`) VALUES
(68, 'Bamboo', 'Dracaena sanderiana', 'The lucky bamboo is one of the quintessential houseplants for enhancing your living space. It’s not hard to see why, either. Their stems naturally grow straight but can also be trained to grow in sweeping curves or tighter spirals. Several long, thin leaves grow out of the end of these stems, making for a visually dynamic plant even without the flowers that only occur in nature.\n\nNamed for its connection to feng shui and the belief that the plant offers good fortune, the Chinese name for the plant (fu gui zhu) means bamboo with fortune and power. Yet, this houseplant is not a true bamboo but is instead a member of the plant genus dracaena. The botanical name for the lucky bamboo species is dracaena sanderiana.', 1),
(70, 'Lily', 'Spathiphyllym', 'The peace lily is among the most famous of all houseplants, and it’s not hard to understand why. It’s easy to grow and hard to kill. It has large, dark, evergreen leaves that grow out in a spray pattern all of which gives the peace lily the “classic houseplant look.” With a good spot and consistent care, you will also get the extra reward of a special white flower bloom, known as a spathe and spadix. Whether you’re starting a new houseplant hobby or adding a new type to your collection, the peace lily is a great choice.', 1),
(71, 'Succulent', 'Aloe Vera', 'The aloe vera plant has long, thick leaves that grow out in a mostly vertical spray pattern. It’s one of the most popular easy-care houseplants, especially for windowsills where it can receive plenty of light. More than just easy to care for, aloe vera houseplants are easy to place and easy to propagate. The plant leaves offer plenty of space to water the soil and visualize any problems early on. It’s also slow growing, which means it won’t obscure a wall hanging or overwhelm a desk or table, even after a few seasons of growth.', 0.25),
(72, 'Succulent', 'Coral Cactus', 'Coral Cactus, or also known as Euphorbia lactea Cristata, is an incredibly unique succulent. It pretty much looks like a large coral reef with a green thick stem and crinkled cabbage-like leaves, which come in either purple, green, ruby, white, or yellow color edges and can only reach up to 2 feet tall when grown in a container.', 0.25),
(73, 'Marantaceae', 'Maranta Tricolor', ' Named for Bartolomeo Maranta, an Italian physician and botanist of the sixteenth century, the Maranta genus includes a few dozen low-growing plants native to Brazil, among them the prayer plant (Maranta leuconeura). It gets its common name from the fact that its leaves stay flat during the day and then fold up like praying hands at night.\n\nThe prayer plant is one of the most distinguishable tropicals, thanks to its beautiful decorative leaves. The popular tricolor variety has deep green, velvety leaves with yellow splotches down the midrib and arching red veins traveling to the leaf margins. A slow-grower, the prayer plant can eventually reach up to a foot in height indoors. They are fairly common as houseplants and can be planted and cared for indoors during any time of the year, but they&#39;re not necessarily easy to keep growing over the long term. ', 1),
(74, 'Philodendron', 'Monstera Deliciosa', ' Native to the rainforests of Central America, the big, bold Monstera deliciosa plant is also known as the &#34;split-leaf philodendron.&#34; This eye-catching climbing evergreen is a popular, easy-to-grow houseplant, and a favorite of many interior designers for both residential and commercial spaces.\n\nIndoors, the plant has a moderate growth rate and can grow in height about 1 to 2 feet a year. Its leathery, glossy, characteristic split, and heart-shaped leaves come from intricate aerial roots, which can be used for ropes and basket-making. It is also called the Swiss cheese plant because of its perforated leaves, which can grow to 3 feet long.\n\nPlant outdoors in the right zone at any time during the year and it will also produce tannish-cream flowers pollinated by bees and edible juicy fruit with the combined flavor of pineapple and banana. However, fruiting is not common in houseplants', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `plants_diseases`
--

CREATE TABLE IF NOT EXISTS `plants_diseases` (
  `id` int NOT NULL AUTO_INCREMENT,
  `plant_id` int NOT NULL,
  `disease_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `plant_id` (`plant_id`,`disease_id`) USING BTREE,
  KEY `disease_id` (`disease_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Zrzut danych tabeli `plants_diseases`
--

INSERT INTO `plants_diseases` (`id`, `plant_id`, `disease_id`) VALUES
(34, 68, 10),
(33, 68, 12),
(39, 70, 10),
(40, 70, 11),
(38, 70, 12),
(42, 71, 1),
(41, 71, 11),
(43, 72, 1),
(44, 72, 12),
(46, 73, 8),
(45, 73, 12),
(47, 74, 1),
(48, 74, 8);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `session_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `is_admin`, `session_id`) VALUES
(2, 'testuser', '$2b$12$krq0/3NBfHoBMyuFXuxLEOvyyyjGuA6r3it.SNw6NJBjmReqw8dhe', 'test23f@dveasdsta.fcom', 1, 'd01cd1d9671e69f6618bd65b7a660ca3'),
(5, 'None1', '$2b$12$.4Sbo/jWa9WeM3PNJzuY8.19LTRFdEB/z1GhtLrFmE7DWom4k4ybK', 'None@test.com', 1, NULL),
(6, 'None13', '$2b$12$BkdtxD2J7jv1wSmqTd1nb.RWcr6vd4jOTGpm7Th2ZVj9z2Tmv/Wdi', 'None@tesr1223.com', 0, NULL),
(7, 'new_user', '$2b$12$GsV.Rvgmnz4ubdeyJN5Ce.JYguPoOcQ3NUgRNSjecxnVQ4EcEMNsu', 'new@test.com', 0, NULL),
(8, 'new_user2', '$2b$12$yxrNnKsrJzSMjqNH8e1K6O2JVnAKUnDSEeeq5P6pKN3cOJKre6qei', 'new@test2.com', 0, NULL),
(9, 'test_user4', '$2b$12$hNx03HYdVi14I98G29fsmOobsQSXcLeKvObKJYIbBY5BiCnyoRIuC', 'test@test4.com', 0, NULL),
(10, 'test_user5', '$2b$12$biwrAnjEubRVoUaqUGJdXO3X03CbFQJkR8TrHb0Wtb7wl7I9bE7cy', 'test@test.com', 0, NULL),
(11, 'test_user6', '$2b$12$puEGK/yz37ktRlrc7dFjeulg90SR0iRY.jcdLYewuF0SkMJDuSuca', 'test1234@test.com', 0, NULL),
(12, 'test6666', '$2b$12$OmKL.HujIfvThBPq5p96zucZgUOZoGkJmrgT2ENeUnvh730bldnay', '3333@rest.com', 0, NULL),
(13, 'test222222', '$2b$12$nYHmg6jGuTCW4dL9vtRWLe6Y.czfPZjmV3D1TFu/XkSVjTqouwPpK', '22@test.com', 0, NULL),
(14, 'test0000', '$2b$12$qMn58UvnOhy0BdCrF/aWHeQFztqprF4/liWqZHDLxnn5yzRI9rhQS', 'test3@fkeo.com', 0, NULL),
(15, 'test123123', '$2b$12$Ha8gy0wIXNknNFaXVrFk0eVWed5vQa9sBwvk7mfHRToRRaKXQFEOi', '2312Wte@test.com', 0, '82b2a62315aeb5a92ce6b7545e09294c'),
(16, 'test1231232', '$2b$12$IXwOY/bh5rn1/0tBOWY7mepoz4SBZ3Jad7nZ116NQjaGGs9ba1uLm', '2312Wte@te2st.com', 0, NULL),
(17, 'testbasic', '$2b$12$sv6JLsqCC/PfYNBnUE.DO.YjbeGZqieWRWlEbcBD6YTocVJm/j0Le', 'bas@vas.com', 0, ''),
(18, 'dasdasd', '$2b$12$5DCoU24NQMTxP63ZAIZhCuCwsrxCRta1qpB//lOjeU9j9rO8ZOBiS', 'dad@dawd.com', 0, NULL),
(19, '123123123', '$2b$12$z1Gt9CungRYDSvPETAN2YeS.HqJqmYu1TwaSAi5FUzU61V1.y.zh2', '213@dsa.com', 0, NULL),
(20, 'werwrew', '$2b$12$A8tCaVfm8IHh2uNXyn/5ueoBfbtUUBJQnAaeutEKGS9jLSrk6Qyaa', 'rewr@dsad.com', 0, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_plants`
--

CREATE TABLE IF NOT EXISTS `user_plants` (
  `id` int NOT NULL AUTO_INCREMENT,
  `plant_id` int NOT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `plant_id` (`plant_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Zrzut danych tabeli `user_plants`
--

INSERT INTO `user_plants` (`id`, `plant_id`, `user_id`, `photo`, `creation_date`) VALUES
(9, 74, 2, 'http://plantbased.dev:5000/_uploads/photos/031696f3-ea4e-45ba-ae0c-1ff78ed9609c.jpg', '2021-12-13 11:11:55'),
(10, 73, 2, 'http://plantbased.dev:5000/_uploads/photos/251fe497-5b0a-499d-a16d-34f4ebe6d368.jpg', '2021-12-13 11:12:26'),
(11, 71, 2, 'http://plantbased.dev:5000/_uploads/photos/82d7c90b-3087-4cc8-b104-0f3c73bbf9f6.jpg', '2021-12-13 11:13:35'),
(12, 71, 2, 'http://plantbased.dev:5000/_uploads/photos/9a48db47-7671-44f8-90fd-487bb770ccde.jpg', '2021-12-13 11:13:42');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `diseases`
--
ALTER TABLE `diseases` ADD FULLTEXT KEY `name_2` (`name`,`description`,`treatment`);

--
-- Indeksy dla tabeli `plants`
--
ALTER TABLE `plants` ADD FULLTEXT KEY `type` (`type`,`specie`,`description`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users` ADD FULLTEXT KEY `fulltext` (`username`,`email`);

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `plants_diseases`
--
ALTER TABLE `plants_diseases`
  ADD CONSTRAINT `plants_diseases_ibfk_1` FOREIGN KEY (`plant_id`) REFERENCES `plants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `plants_diseases_ibfk_2` FOREIGN KEY (`disease_id`) REFERENCES `diseases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `user_plants`
--
ALTER TABLE `user_plants`
  ADD CONSTRAINT `user_plants_ibfk_1` FOREIGN KEY (`plant_id`) REFERENCES `plants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_plants_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
