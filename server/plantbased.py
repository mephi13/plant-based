import json
import os
import time
import hashlib

from flask import Flask, request, jsonify, flash, escape
from flask_uuid import uuid
from flask_uploads import UploadSet, configure_uploads, IMAGES, patch_request_class,UploadNotAllowed
from flask_bcrypt import generate_password_hash, check_password_hash
from flask_admin.contrib.fileadmin import FileAdmin
from mysql.connector.conversion import MySQLConverter
import os.path as op

from requests.compat import urlparse
from flask_login import login_user, login_required, logout_user, current_user
from db.models import User, Plant, Disease
from flask_cors import CORS
from flask_mysqldb import MySQL
from config import BaseConfig
from db import initialize_db
from sessions import initialize_login_manager, initialize_bcrypt, bcrypt, login_manager
import wtforms_json

IMG_BASE_PATH = "/img"


# konfiguracja
app = Flask(__name__)
app.config.from_object(BaseConfig)
wtforms_json.init()

#init połączenia z bazą danych mysql
connpool = initialize_db(app)

#init menadżera logowań
initialize_login_manager(app)
User.conn = connpool.get_connection()

#inicjalizacja algorytmu haszującego bcrypt oraz modułu generacji uuid
initialize_bcrypt(app)

#konfiguracja uploadów
photos = UploadSet('photos', IMAGES)
configure_uploads(app, photos)
patch_request_class(app)  

#definicja funkcji sprawdzającej zajętość nazwy użytkownika i mejla
def check_existance(table, col, val):
    query = f'SELECT `{col}` FROM `{table}` WHERE `{col}` = %s'.format()
    app.logger.debug(query)
    conn = connpool.get_connection()
    cursor = conn.cursor(prepared=True)
    cursor.execute(query, (val,))
    response = cursor.fetchone()
    app.logger.debug(response)
    conn.close()
    return response

from forms import RegistrationForm, LoginForm, NewPlantForm, UploadForm
from admin import admin, MysqlView, initialize_admin, admin

#utworzenie widoków bazy danych
MysqlView.conn =  connpool.get_connection()

#inicjalizacja interfejsu administratora
initialize_admin(app)

#Inicjalizuj widok plików
path = op.join(op.dirname(__file__), 'static')
admin.add_view(FileAdmin(path, '/static/', name='Static Files'))

#Inicjalizacja konwertera dla mysql
converter = MySQLConverter(True)

# uruchomienie CORS
CORS(app, resources={r'/*': {'origins': '*'}})

#Definicja funkcji do logowania użytkowanika na podstawie tokenu
def get_user_id(session_id):
        query = """SELECT `username`,`email`,`is_admin`,`id` FROM `users` WHERE `session_id` = (%s)"""
        try:
            conn = connpool.get_connection()
            cursor = conn.cursor(prepared=True)
            cursor.execute(query, (session_id, ) ) 
            response = cursor.fetchone()
            conn.close()
            if not response:
                return None

            user = User.from_query(response)
            return user
        except Exception as ex:
            app.logger.debug("error in get user id: " + str(ex))
            return None
User.get = get_user_id


#############################
##ŚCIEŻKI I DEFINICJE AKCJI##
#############################


# sanity check route
@app.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify('pong!')

@app.route('/login', methods=['POST'])
def login():
    """logowanie użytkownika"""
    form = LoginForm.from_json(request.json)
    if form.validate_on_submit():
        app.logger.debug(form.data)
        user = User()
        conn = connpool.get_connection()
        #Zapytanie do bazy danych
        query = """SELECT password,id,email FROM `users` WHERE `username`= %s OR `email` = %s"""
        name = form.data["username"]
        password = form.data["password"]

        cursor = conn.cursor(prepared=True)
        cursor.execute(query, (name,name))
        response = cursor.fetchall()

        if not response:
            conn.close()
            return jsonify({"status": 401,
                        "reason": {"auth": ["Username and Password pair doesn't exist"]}})

        #Sprawdzanie haszu hasła
        user.authenticated = check_password_hash(response[0][0], password)
        if not user.authenticated:
            conn.close()
            return jsonify({"status": 401,
                    "reason": {"auth":["Username and Password pair doesn't exist"]}})

        #Logowanie poprawne, wypełnij pola użytkownika
        user.id = response[0][1]
        user.username = name
        user.email = response[0][2]

        #Wygeneruj id sesji
        session_id_prehash = str(user.id).encode() + str(time.time_ns()).encode() + os.urandom(16)
        user.session_id = hashlib.md5(session_id_prehash).hexdigest()
        app.logger.debug(user.session_id)

        #Zapisz id sesji w bazie danych
        query = """UPDATE `users` SET `session_id`=%s WHERE `id` = %s"""
        cursor = conn.cursor(prepared=True)
        cursor.execute(query, (user.session_id,user.id))
        conn.commit()
        conn.close()

        #Zaloguj użytkownika
        login_user(user, remember=form.remember)
        response = user.to_json()
        response.update({"status": 200})
        print(form.data)
        if form.data['next']:
            response.update({"next": form.data['next']})
        app.logger.debug(response)
        return jsonify(response)
    return jsonify({"status": 401,
                    "reason": form.errors})

@app.route('/isAuthenticated', methods=['GET'])
def is_authenticated():
    """Sprawdź uwierzytelnienie użytkownika"""
    return jsonify({"authenticated": current_user.authenticated if hasattr(current_user, "authenticated") else False})

@app.route('/isAuthorized', methods=['GET'])
def is_authorized():
    """Sprawdź autoryzację użytkownika"""
    return jsonify({"authorized": current_user.is_admin if hasattr(current_user, "is_admin") else False})

@app.route('/getUser', methods=['GET'])
@login_required
def get_uset_data():
    """Pobierz dane użytkownika"""
    response = current_user.to_json()
    response.update({"status": 200})
    return jsonify(response)


@app.route('/logout', methods=['GET'])
@login_required
def logout():
    """Wyloguj aktualnego użytkownika"""
    user = current_user
    user.authenticated = False
    conn = connpool.get_connection()
    app.logger.debug("User: " + str(current_user.username) + " is logging out...")
    try:
        #usun id sesji z bazy danych
        query = """UPDATE `users` SET `session_id`=%s WHERE `id` = %s"""
        cursor = conn.cursor(prepared=True)
        cursor.execute(query, ("",user.id))
        conn.commit()
        conn.close()

        logout_user()
        return jsonify({"msg": "User logged out successfully", "status": 200})
    except Exception as ex:
        app.logger.debug(str(ex))
        conn.close()
        return jsonify({"reason": "User not logged out", "status": 500})

@app.route('/register', methods=['POST'])
def register():
    """Zarejestruj nowego użytkownika"""
    form = RegistrationForm.from_json(request.json)
    app.logger.debug(form.data)
    if form.validate_on_submit():
        user = User()
        user.username = form.username.data
        user.email = form.email.data
        app.logger.debug(user.username + " " + user.email)
        password = generate_password_hash(form.password.data)

        conn = connpool.get_connection()

        #Dopisz użytkownika do bazy danych
        query = """INSERT INTO `users`(`username`, `password`, `email`) VALUES (%s, %s, %s)"""
        cursor = conn.cursor(prepared=True)
        cursor.execute(query, (user.username, password, user.email))
        conn.commit()
        conn.close()
        
        return jsonify({"msg": "User registered successfully",
                        "next": "/login",
                        "status": 200})
    return jsonify({"status": 401,
                "reason": form.errors})


def query_sql_dynamic(query, params=(), commit=False, prepared=True):
    conn = connpool.get_connection()
    cursor = conn.cursor(prepared=prepared)
    if prepared:
        print(query%params)
        cursor.execute(query, params)
    else:
        cursor.execute(query)
    if commit:
        conn.commit()
    last_id = cursor.lastrowid
    try:
        result = cursor.fetchall()
        conn.close()
    except:
        conn.close()
    if not commit:
        return [dict(zip(list(cursor.column_names), [y for y in x] )) for x in result]
    else:
        return last_id

def paginate(list, page, size = 10):
    return list[page*size: (page+1)*size :]

@app.route('/getPlants', methods=['GET', 'POST'])
def get_plants():
    query = "SELECT plants.id as id ,type,specie,plants.description as description,watering_weekly,GROUP_CONCAT(diseases.name) as diseases \
                    FROM `plants` \
                    LEFT JOIN plants_diseases \
                        ON plants_diseases.plant_id = plants.id \
                    LEFT JOIN diseases \
                        ON plants_diseases.disease_id = diseases.id "
    result = []
    try:
        data = request.get_json(force=True)
        if data:
            if data['search']:
                searchable_columns = ['plants.id', 'type', 'specie' , 'plants.description']
                for column in searchable_columns:
                    new_query = query +  """WHERE %s"""  % (column,) + " LIKE %s GROUP BY plants.id"
                    response = (query_sql_dynamic(new_query, ("%" + str(escape(data['search']))  +"%",), prepared=True))
                    result += response
                #Fikolek dla group_concat
                new_query = query +  """GROUP BY plants.id HAVING %s"""  % ("diseases",) + " LIKE %s "
                response = (query_sql_dynamic(new_query, ("%" + str(escape(data['search']))  +"%",), prepared=True))
                result += response

                result =list({frozenset(item.items()) : item for item in result}.values()) 
            else:
                query += "GROUP BY plants.id"
                result = query_sql_dynamic(query, prepared=False)
            
            if (data and "page" in data):
                page = data["page"]
                size = data["size"] if "size" in data else 10
                result = paginate(result, page, size)
        else:
            query += "GROUP BY plants.id"
            result = query_sql_dynamic(query, prepared=False)
        return(jsonify(result))
    except Exception as ex:
        app.logger.debug(str(ex))
        return jsonify({"reason": "Can't get plants data", "status": 500})

@app.route('/getPlant', methods=['GET', 'POST'])
def get_plant():
    query = """SELECT plants.id as id ,type,specie,plants.description as description,watering_weekly,GROUP_CONCAT(diseases.name) as diseases \
                    FROM `plants` \
                    LEFT JOIN plants_diseases \
                        ON plants_diseases.plant_id = plants.id \
                    LEFT JOIN diseases \
                        ON plants_diseases.disease_id = diseases.id \
                    WHERE plants.id = %s \
                    GROUP BY plants.id """
    data = request.get_json(force=True)
    try:
        if not request.data or not  data or not data["id"]:
            return jsonify({"status": 400,
                    "reason": "Plant id is required"})
        print(data["id"])
        result = query_sql_dynamic(query, (data["id"],), prepared=True)
        print(result)
        return(jsonify(result))
    except Exception as ex:
        app.logger.debug(str(ex))
        return jsonify({"reason": "Can't get plant data", "status": 500})

@app.route('/getDiseases', methods=['GET', 'POST'])
def get_diseases():
    try:
        query = "SELECT *  \
                        FROM `diseases`"
        result = query_sql_dynamic(query)
        if request.data:
            data = request.get_json(force=True)
            if (data and "page" in data):
                page = data["page"]
                size = data["size"] if "size" in data else 10
                result = paginate(result, page, size)
        return(jsonify(result))
    except Exception as ex:
        app.logger.debug(str(ex))
        return jsonify({"reason":str(ex), "status": 500})

@app.route('/getDisease', methods=['GET', 'POST'])
def get_disease():
    query = """SELECT  diseases.id as id,name,diseases.description as description,treatment \
                    FROM `diseases` \
                    INNER JOIN `plants_diseases` \
                        ON plants_diseases.disease_id = diseases.id \
                    INNER JOIN `plants` \
                        ON plants_diseases.plant_id = plants.id \
                    WHERE plants.id = %s"""
    data = request.get_json(force=True)
    try:
        if not request.data or not  data or not data["id"]:
            return jsonify({"status": 400,
                    "reason": "Plant id is required"})
        print(data["id"])
        result = query_sql_dynamic(query, (data["id"],), prepared=True)
        print(result)
        return(jsonify(result))
    except Exception as ex:
        app.logger.debug(str(ex))
        return jsonify({"reason": "Can't get disease data", "status": 500})

@app.route('/getPlantImages', methods=['GET', 'POST'])
def get_plant_images():
    data = request.get_json(force=True)
    if not  data or not data["id"]:
        return jsonify({"status": 400,
                "reason": "Plant id is required"})
    try:
        query = """SELECT photo FROM `user_plants` WHERE plant_id = %s"""
        result = query_sql_dynamic(query, (escape(data["id"]),), prepared=True)
        response = {'status': 200, 'paths': result}
        return jsonify(response)
    except Exception as ex:
        app.logger.debug(str(ex))
        return jsonify({"reason": "Error while fetching image paths", "status": 500})

@app.route('/uploadImage', methods=['GET', 'POST'])
@login_required
def uploadImage():
    form = UploadForm()
    if form.validate_on_submit():
        try:
            print(form.photo.data)
            filename = photos.save(form.photo.data, name=  """%s""" % (uuid.uuid4(),) + os.path.splitext(form.photo.data.filename)[1] )
            file_url = photos.url(filename)
            response = {'status': 200, 'path': file_url}
            return response
        except Exception as ex:
            return jsonify({"status": 400,
                "reason": str(ex)})
    else:
        return jsonify({"status": 401,
            "reason": form.errors})

@app.route('/addUserPlant', methods=['GET', 'POST'])
@login_required
def add_user_plant():
    from forms import NewUserPlant

    form = NewUserPlant.from_json(request.json)
    app.logger.debug(form.data)
    if form.validate_on_submit():
        try:
            filename = ((urlparse(form.photo.data).path).split('/'))[-1]
            query =  """INSERT INTO `user_plants`(`plant_id`, `user_id`, `photo`)VALUES (%s,%s,%s)"""
            result = query_sql_dynamic(query, (form.plant_id.data, \
                current_user.id, form.photo.data), commit=True)

            return{'status': 200, 'message': "Plant added"}
        except Exception as ex:
            return {'status': 400, 'reason': str(ex)}
    else:
        return jsonify({"status": 401,
            "reason": form.errors})


    
@app.route('/addPlant', methods=['GET', 'POST'])
@login_required
def add_plant():
    import shutil
    if not current_user.is_admin:
        return current_app.login_manager.unauthorized()

    form = NewPlantForm.from_json(request.json)
    app.logger.debug(form.data)
    if form.validate_on_submit():
        try:
            query =  """INSERT INTO `plants`( `type`, `specie`, `description`, `watering_weekly`) VALUES (%s,%s,%s,%s)"""
            result = query_sql_dynamic(query, (form.type.data, \
                form.specie.data, form.description.data,form.watering_weekly.data ), commit=True)
           
            filename = ((urlparse(form.photo.data).path).split('/'))[-1]
            feaured_path = os.path.abspath(os.path.dirname(__file__)) + '/static/img/featured/' 
            if os.path.isfile(feaured_path+ str(result)):
                os.remove(feaured_path+ str(result)) 
            shutil.copy(photos.path(filename), feaured_path + str(result))

            if form.diseases.data:
                for disease in form.diseases.data:
                    query = """INSERT INTO `plants_diseases`(`plant_id`,`disease_id`) VALUES ( %s, (SELECT `diseases`.`id` from `diseases` where `name` = %s))"""
                    params =(result, disease)
                    query_sql_dynamic(query, params, commit=True)
            return{'status': 200, 'message': "Plant added"}
        except Exception as ex:
            return {'status': 400, 'reason': str(ex)}
    else:
        return jsonify({"status": 401,
            "reason": form.errors})


@app.route('/getMyPlants', methods=['GET', 'POST'])
@login_required
def get_my_plants():
    query = "SELECT plants.id as id,type,specie,plants.description as description,watering_weekly, creation_date as date,\
                        GROUP_CONCAT(diseases.name) as diseases, user_plants.photo as photo, user_plants.id as unique_id \
                    FROM `plants` \
                    INNER JOIN user_plants \
                        ON user_plants.plant_id = plants.id \
                    LEFT JOIN plants_diseases \
                        ON plants_diseases.plant_id = plants.id \
                    LEFT JOIN diseases \
                        ON plants_diseases.disease_id = diseases.id \
                    WHERE user_plants.user_id = %s "
    result = []
    try:
        data = request.get_json(force=True)
        if data:
            if data['search']:
                searchable_columns = ['plants.id', 'type', 'specie' , 'plants.description']
                for column in searchable_columns:
                    new_query = query +  """AND %s"""  % (column,) + " LIKE %s GROUP BY user_plants.id"
                    response = (query_sql_dynamic(new_query, (current_user.id, "%" + str(escape(data['search']))  +"%"), prepared=True))
                    result += response
                #Workarond for group concat
                new_query = query +  """GROUP BY user_plants.id HAVING %s"""  % ("diseases",) + " LIKE %s "
                response = (query_sql_dynamic(new_query, (current_user.id, "%" + str(escape(data['search']))  +"%"), prepared=True))
                result += response

                result =list({frozenset(item.items()) : item for item in result}.values()) 
            else:
                query += "GROUP BY user_plants.id"
                result = query_sql_dynamic(query, (current_user.id,), prepared=True)
            print(result)


            if ("page" in data):
                page = data["page"]
                size = data["size"] if "size" in data else 10
                result = paginate(result, page, size)
        else:
            query += "GROUP BY user_plants.id"
            result = query_sql_dynamic(query, (current_user.id,), prepared=True)
    
        return(jsonify(result))
    except Exception as ex:
        app.logger.debug(str(ex))
        return jsonify({"reason": "Can't get plants data", "status": 500})

@app.after_request
def middleware_for_response(response):
    # Allowing the credentials in the response.
    response.headers.add('Access-Control-Allow-Credentials', 'true')
    #print(response.headers)
    return response

if __name__ == '__main__':
    app.run()
