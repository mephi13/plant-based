from flask_wtf import FlaskForm
from flask import escape
from wtforms import StringField, PasswordField, BooleanField, \
    FloatField, IntegerField, TextField, Field, FileField, SubmitField
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms.validators import InputRequired, URL, \
    Length, EqualTo, Regexp, Email, ValidationError, Optional
from flask_bcrypt import generate_password_hash
from plantbased import check_existance
from flask_uploads import IMAGES


class LoginForm(FlaskForm):
    username = StringField('username', 
        validators=[InputRequired(), 
            Length(min=4, max=25), 
            Regexp("^[a-zA-Z0-9\-_.,!@#$%^*()]+$", message="Username contains illegal character(s)")], 
        filters=[escape])
    password = PasswordField('password', validators=[InputRequired()])
    remember = BooleanField('remember me', validators=[])
    next = StringField('next', filters=[], validators=[Optional(), Regexp("^(\/{1}).*"), Length(max=50)])

class CheckExistance(object):
    def __init__(self, table="users", name=None, message=None, should_exist=False):
        self.table = table
        self.name = name
        self.should_exist = should_exist
        if not message:
            message = '%s should be unique and is already in use' 
        self.message = message

    def __call__(self, form, field):
        if self.name:
            name = self.name
        else:
            name = field.name
        if type(field.data) == list:
            if not field.data[0]:
                return True
            for item in field.data:
                print(item)
                exists = check_existance(self.table, name, item)
                if (exists and not self.should_exist) or (not exists and self.should_exist):
                 raise ValidationError(self.message % (name,))
        else:
            if not field.data:
              return True
            exists = check_existance(self.table, name, field.data)
            if (exists and not self.should_exist) or (not exists and self.should_exist):
                raise ValidationError(self.message % (name,))

class RegistrationForm(FlaskForm):
    username = StringField('username', 
        validators=[InputRequired(), 
            Length(min=4, max=25), 
            Regexp("^[a-zA-Z0-9\-_.,!@#$%^*()]+$", message="Username contains illegal character(s)"),
            CheckExistance()], 
        filters=[escape])

    email = StringField('email', 
        validators=[
            InputRequired(), 
            Length(min=3, max=35),
            Email(),
            CheckExistance()], 
        filters=[escape])

    password = PasswordField('new password', [
        Length(min=8, max=64),
        InputRequired(),
        EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('repeat password', validators=[InputRequired()])

def  generate_hash_wrapper(pw):
    return generate_password_hash(pw).decode()

class AdminUserForm(RegistrationForm):
    password = PasswordField('new password', [
        Length(min=8, max=255),
        InputRequired(),
    ],
    filters=[generate_hash_wrapper])
    confirm = PasswordField('repeat password', validators=[InputRequired()])
    is_admin = BooleanField('admin')

class AdminEditUserForm(FlaskForm):
    username = StringField('username', 
        validators=[InputRequired(), 
            Length(min=4, max=25), 
            Regexp("^[a-zA-Z0-9\-_.,!@#$%^*()]+$", message="Username contains illegal character(s)"),
            ], 
        filters=[])
    email = StringField('email', 
        validators=[InputRequired(), 
            Length(min=3, max=35),
            Email(),
            ], 
        filters=[])
    password = PasswordField('new password', [Optional(),
        Length(min=8, max=64)
    ],
    filters=[generate_hash_wrapper])
    is_admin = BooleanField('admin')

class AdminPlant(FlaskForm):
    type = StringField('Type',filters=[escape])
    specie = StringField('Specie',validators=[] ,filters=[escape])
    description = StringField('Description', filters=[escape])
    watering_weekly =  FloatField('Weekly watering', filters=[])

class AdminDisease(FlaskForm):
    name = StringField('Name',validators=[], filters=[escape])
    description = StringField('Description', filters=[escape])
    treatment = StringField('Treatment', filters=[escape])

class AdminPlantDisease(FlaskForm):
    plant_id = IntegerField("Plant id")
    disease_id = IntegerField("Disease id")

class AdminUserPlant(FlaskForm):
    plant_id =IntegerField("Plant id")
    user_id =IntegerField("User Id")
    photo =StringField("photo")

class DiseaseList(Field):
    widget = TextField()
    def _value(self):
        if self.data:
            return u', '.join(self.data)
        else:
            return u''
    def process_formdata(self, valuelist):
        if valuelist:
            self.data = [x.strip() for x in valuelist[0].split(',')]
        else:
            self.data = []

class NewPlantForm(FlaskForm):
    type = StringField('Type', validators=[
        InputRequired(), 
        Length(min=1, max=255), 
    ],filters=[escape])
    specie = StringField('Specie',validators=[
        InputRequired(), 
        Length(min=1, max=255), 
        CheckExistance(table="plants")
    ] ,filters=[escape])
    description = TextField('Description',  validators=[
        InputRequired(), 
        Length(min=1, max=1000), 
    ],filters=[escape])
    watering_weekly =  FloatField('Weekly waterings amount', filters=[])
    diseases = DiseaseList("Diseases, comma separated",validators=[
        Length(min=1, max=1000), 
        CheckExistance(message="Disease %s(s) not in database",name="name", table='diseases', should_exist=True)
    ],filters=[])
    photo = StringField('Photo', validators=[InputRequired()
    ],filters=[])

class UploadForm(FlaskForm):
    photo = FileField("Feature image", validators=[FileAllowed(IMAGES, 'Image only!'), FileRequired('File was empty!')])
    #submit = SubmitField('Upload')

class NewUserPlant(FlaskForm):
    photo = StringField('Photo', validators=[InputRequired()],filters=[escape])
    plant_id = IntegerField("Plant id", validators=[InputRequired()],)
    