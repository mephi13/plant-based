from flask_login import LoginManager,login_user
from db.models import User
from flask_uuid import FlaskUUID
from flask_bcrypt import Bcrypt

login_manager = LoginManager()
login_manager.login_view = 'http://plantbased.dev:8080/login'
login_manager.session_protection = "strong"

bcrypt = Bcrypt()
#uuid = FlaskUUID()

def initialize_login_manager(app):
    app.logger.info("Initializing login manager...")
    login_manager.init_app(app)

def initialize_bcrypt(app):
    bcrypt.init_app(app)

@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)
