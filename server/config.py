import os
from datetime import timedelta

class BaseConfig(object):
    SECRET_KEY = os.environ['SECRET_KEY']
    DEBUG = True
    MYSQL_DB = os.environ['DB_NAME']
    MYSQL_HOST = os.environ['DB_HOST']
    MYSQL_PORT = int(os.environ['DB_PORT'])
    MYSQL_USER = os.environ['DB_USER']
    MYSQL_PASSWORD = os.environ['DB_PASS']
    REMEMBER_COOKIE_DURATION=timedelta(days = 7)
    FLASK_ADMIN_SWATCH = 'cyborg'
    WTF_CSRF_ENABLED = False
    REMEMBER_COOKIE_DOMAIN='.plantbased.dev:8080'
    SESSION_COOKIE_Domain='plantbased.dev'
    SESSION_COOKIE_SAMESITE = 'None'
    SESSION_COOKIE_SECURE = False
    REMEMBER_COOKIE_SAMESITE = 'None'
    SERVER_NAME = "plantbased.dev:5000"
    UPLOADED_PHOTOS_DEST = os.path.abspath(os.path.dirname(__file__)) + '/static/img' #
    
    #ADMIN_PASS = os.environ['ADMIN_PASS']

