#Tu zdefiniowane są modele rekordów w bazie danych
from . import mysql
from flask_login import UserMixin
from mysql.connector import cursor
from dataclasses import dataclass, fields, astuple
import json

@dataclass
class MysqlModel():
    table_name: str
    primary_column: str
    column_names: list
    searchable_columns: str
    sortable_columns: str
    model_class: object
    __name__:"MySQL Model"

@dataclass
class StandardModel():
    @classmethod
    def list_attributes(self):
        return [x.name for x in fields(self)]
    def to_json(self):
        return vars(self)

@dataclass
class User(UserMixin):
    id: int = 0
    username: str = ""
    email: str = ""
    password: str = ""
    is_admin: bool = False
    authenticated: bool = False
    session_id: str = ""

    def __post_init__(self):
        self.is_admin = bool(self.is_admin)

    def to_json(self):
        return {"username": self.username,
                "email": self.email}
    def is_authenticated(self):
        return self.authenticated
    def is_active(self):
        return True
    def is_anonymous(self):
        return False
    def get_id(self):
        return self.session_id
    @staticmethod
    def from_query(response: tuple):
        user = User()
        try:
            user.username = response[0]
            user.email = response[1]
            user.id = response[3]
            user.is_admin = response[2]
            user.authenticated = True
        except:
            raise ValueError("Bad query response")
        return user
    #To będzie zdefiniowane później
    @staticmethod
    def get(userid):
        pass

@dataclass
class Plant(StandardModel):
    id: int = 0
    type: str = ""
    specie: str = ""
    description: str = ""
    watering_weekly: float = 0.0

@dataclass
class Disease(StandardModel):
    id: int = 0
    name: str = ""
    description: str = ""
    treatment: str = ""

@dataclass
class PlantDisease(StandardModel):
    id: int = 0
    plant_id: int = 0
    disease_id: int = 0

@dataclass
class UserPlant(StandardModel):
    id: int = 0
    plant_id: int = 0
    user_id: int = 0
    photo: str = ""
    creation_date: str = ""

users_model = MysqlModel("users", "id", 
["id", "username", "password", "email", "is_admin", "session_id"],
["email","username"], ["id","email","username","is_admin"],model_class=User)

plants_model = MysqlModel("plants", "id", 
Plant.list_attributes(),
["type", "specie", "description"], 
["type", "specie", "watering_weekly"],
model_class=Plant)

user_plant_model=MysqlModel("user_plants", "id", 
UserPlant.list_attributes(),
UserPlant.list_attributes(),
UserPlant.list_attributes(),
model_class=UserPlant)

plants_diseases_model=MysqlModel("plants_diseases", "id", 
PlantDisease.list_attributes(),
PlantDisease.list_attributes(),
PlantDisease.list_attributes(),
model_class=PlantDisease)

diseases_model = MysqlModel("diseases", "id", 
Disease.list_attributes(),
["name", "description", "treatment"], 
["name"],
model_class=Disease)