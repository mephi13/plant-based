import mysql.connector
import mysql.connector.pooling

def initialize_db(app):
    app.logger.info("Initializing MySQL connection...")
    connpool = None
    while not connpool:
        try:
            connpool = mysql.connector.pooling.MySQLConnectionPool(pool_name = "mypool",
                                                                pool_size = 10, 
                                                                host=app.config['MYSQL_HOST'],
                                                                port=app.config['MYSQL_PORT'],
                                                                user=app.config['MYSQL_USER'],
                                                                password=app.config['MYSQL_PASSWORD'], 
                                                                database=app.config['MYSQL_DB'])
        except Exception as ex:
            connpool = None
            app.logger.debug(str(ex))
    return connpool
