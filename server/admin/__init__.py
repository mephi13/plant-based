from flask_admin import Admin, AdminIndexView
from flask_admin.model import BaseModelView
from flask_wtf import FlaskForm
#from plantbased import login, login_manager, conn, cursor
from db.models import MysqlModel,User, users_model, \
        Disease, Plant, plants_model, diseases_model, \
        PlantDisease, plants_diseases_model, \
        UserPlant, user_plant_model
from markupsafe import Markup
from flask_bcrypt import generate_password_hash
from flask import jsonify, flash
from flask_login import current_user
import forms
from dataclasses import asdict
import csv

class AdminIndexViewProtected(AdminIndexView):
    def is_accessible(self):
        return current_user.is_admin if hasattr(current_user, "is_admin") else False

admin = Admin(name='plantbased', template_mode='bootstrap4', index_view=AdminIndexViewProtected())

def initialize_admin(app):
    app.logger.info("Initializing admin interface...")
    plants_view = MysqlView(Plant, plants_model, forms.AdminPlant)
    diseases_view = MysqlView(Disease, diseases_model, forms.AdminDisease)
    users_view = MysqlView(User, users_model, forms.AdminUserForm, forms.AdminEditUserForm)
    p_d_view = MysqlView(PlantDisease, plants_diseases_model, forms.AdminPlantDisease)
    u_p_view = MysqlView(UserPlant, user_plant_model, forms.AdminUserPlant)

    admin.add_view(users_view)
    admin.add_view(diseases_view)
    admin.add_view(plants_view)
    admin.add_view(p_d_view)
    admin.add_view(u_p_view)

    admin.init_app(app)


class MysqlView(BaseModelView):
    conn = None
    data_model=None

    def __init__(self, model, data_model, model_form= FlaskForm, model_edit_form = None , **kwargs):
        self.model_form = model_form
        if model_edit_form:
            self.model_edit_form = model_edit_form
        else:
            self.model_edit_form = self.model_form
        self.model = model
        self.data_model = data_model
        BaseModelView.__init__(self, model, **kwargs, name = self._prettify_class_name(model.__name__), )

    def is_accessible(self):
        return current_user.is_admin if hasattr(current_user, "is_admin") else False

    def get_edit_form(self):
        return self.model_edit_form

    def get_pk_value(self, model):
        return model.id

    def scaffold_list_columns(self):
        columns = self.data_model.column_names
        return columns

    def scaffold_sortable_columns(self):
        return self.data_model.sortable_columns

    def init_search(self):
        #self.fulltext_search_query = """SELECT * FROM `users` WHERE MATCH (username, email) AGAINST ('%test%');"""
        return True

    def scaffold_form(self):
        return self.model_form

    def get_list(self, page, sort_field, sort_desc, search, filters, page_size=None):
        try:
            query = ""
            if search:
                query = "SELECT * FROM %s " % (self.data_model.table_name,) + \
                        "WHERE `%s`" + " LIKE '%%%s%%' "
            else:
                query = "SELECT * FROM %s " % (self.data_model.table_name,)
            
            if sort_field and sort_desc:
                query += ("ORDER BY `%s`.`%s` DESC"  % (self.data_model.table_name,sort_field))
            elif sort_field:
                query += ("ORDER BY `%s`.`%s` ASC"  % (self.data_model.table_name,sort_field))

            results = []
            cursor = self.conn.cursor(prepared=True)
            if search:
                for column in self.data_model.searchable_columns:
                    query_p = query % (column,search)
                    cursor.execute(query_p, ())
                    response = cursor.fetchall()
                    for result in response:
                        if not (result in results):
                            results.append(result) 
            else:
                cursor.execute(query, ())
                results = cursor.fetchall()
        except Exception as ex:
            flash("SQL statement failed: " + str(ex))
            return (0, [])
        
        if (page and page_size):
            start_id = page*page_size
            end_id = (page+1)*page_size if (page+1)*page_size < len(results) else len(results) - 1
            results = results[start_id:end_id:1]
        if results:
            #results = [[x.decode() if x and hasattr(x, "decode") else x for x in y] for y in results if y]
            models = [ self.model(**dict(zip(self.data_model.column_names, x))) for x in results if x]
            return (len(models), models)
        return (0, [])

    def get_one(self, id):
        get_query = "SELECT * FROM %s WHERE `%s` = %s" % (self.data_model.table_name, self.data_model.primary_column, id)
        try:
            cursor = self.conn.cursor(prepared=True)
            cursor.execute(get_query, ())
            results = cursor.fetchall()
            result = results[0]
            #result = [x.decode() if x and hasattr(x, "decode") else x for x in result] 
            model  = self.model(**dict(zip(self.data_model.column_names, result))) 
            return model
        except Exception as ex:
            flash("SQL statement failed: " + str(ex), 'error')
            return None

    def create_model(self, form):
        data = form.data
        #del data['csrf_token']   
        data = {k: v for (k, v) in data.items() if (k in self.data_model.column_names)}
        query = self.insert_from_form(data, form)
        cursor = self.conn.cursor(prepared=True)
        try:
            cursor.execute(query, ())
            self.conn.commit()
            return jsonify({"status": "200",
                            "message": "Record successfully created"})
        except Exception as ex:
            flash("SQL statement failed: " + str(ex), 'error')
            return jsonify({"status": "400",
                        "reason": "SQL statement failed: " + str(ex)}), 400

    def update_model(self, form, model):
        data = form.data
        attributes = asdict(model)
        data = {k: v for (k, v) in data.items() if (k in attributes and attributes[k] != data[k] and v != None)}
        if not data:
            return jsonify({"status": "200",
                            "message": "Nothing updated"})
        query, params = self.update_from_form(data, model.id)
        cursor = self.conn.cursor(prepared=True)
        try:
            cursor.execute(query % params, ())
            self.conn.commit()
            return jsonify({"status": "200",
                            "message": "Record updated"})
        except Exception as ex:
            flash("SQL statement failed: " + str(ex), 'error')
            return jsonify({"status": "400",
                        "reason": "SQL statement failed: " + str(ex)}), 400

    def delete_model(self, model):
        query = "DELETE FROM `%s` WHERE `%s`=%s" % (self.data_model.table_name, self.data_model.primary_column, model.id)
        cursor = self.conn.cursor(prepared=True)
        cursor.execute(query, ())
        self.conn.commit()

    def insert_from_form(self, data, form):
        query = """INSERT INTO `%s`(%s) VALUES (%s)"""
        #del data['csrf_token']
        keys = ",".join(map(str, data.keys()))
        values = []
        for value in data.values():
            value = value if not type(value) == Markup else str(value)
            values.append(
                str(value) if not (type(value) == str)
                else ("'" + value + "'"))
        values = ",".join(values)
        return query % (self.data_model.table_name, keys, values)

    def update_from_form(self, data, id):
        query = """UPDATE `%s` SET %s WHERE `id` = %s"""
        data = ["`"+k+"`='"+v+"'" if type(v) == str else "`"+k+"`="+str(v) for (k,v) in data.items()]
        ",".join(data)
        if data and type(data) == list:
            data = data[0]
        return (query, (self.data_model.table_name, data, id))

    


