# Plant Based

Projekt SPA (Single Page Application) o roślinach domowych. Dewelopowany z wykorzystaniem Vue.js (Frontend) oraz Flask (Backend).

## Używanie

### Docker

1. Utworz plik ```.env``` w folderze glownym
2. Dodaj do niego nastepujace zmienne srodowiskowe:

```bash
DB_ROOT_PASSWORD=[haslo_roota_do_bazy_danych]
DB_USER_PASSWORD=[haslo_aplikacji_do_bazy_danych]
```
3. Utworz plik ze zmiennymi srodowiskowymi ```server/.env``` 
4. Dodaj do niego nastepujace zmienne srodowiskowe:

```bash
#.flaskenv
FLASK_APP=plantbased
FLASK_ENV=development
APP_SETTINGS=app.configuration.config.BaseConfig
DEBUG=True
SECRET_KEY=[TWOJ_KLUCZ_DOWOLNY_CIAG_ZNAKOW]
DB_NAME=plantbased
DB_USER=db_user
DB_PASS=[haslo_aplikacji_do_bazy_danych]
DB_HOST=127.0.0.1
DB_PORT=3306
```
5. Dodaj do pliku ```/etc/hosts``` nastepujaca linijke:
```bash
127.0.0.1  plantbased.dev localhost
```
6. Uruchom obrazy z dockera
```bash
docker-compose -f stack.yml up --build
```

Utworzone zostana cztery instancje obrazów dockera, jeden z bazą danych MySQL na porcie 3306 oraz drugi z panelem administratora Adminer na porcie 9090, trzeci z frontendem na porcie 8080 i czwarty z backendem na porcie 5000

Uwaga, w celu prawidlowego dzialania strony trzeba tymczasowo zezwolic na niebezpieczne polaczenia http