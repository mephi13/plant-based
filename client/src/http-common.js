import axios from "axios";

export default axios.create({
  baseURL: "http://plantbased.dev:5000",
  headers: {
    "Content-type": "application/json"
  },
  withCredentials: true
});