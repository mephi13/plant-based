import http from "../http-common";

class Auth {
  login(data) {
    console.log(data)
    return http.post("/login", data);
  }
  logout() {
    return http.get("/logout");
  }
  register(data) {
    return http.post("/register", data);
  }
  isAuthenticated(){
    return http.get("/isAuthenticated");
  }
  isAuthorized(){
    return http.get("/isAuthorized");
  }
  getUser(){
    return http.get("/getUser")
  }
}

export default new Auth();