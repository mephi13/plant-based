import http from "../http-common";

class Plants {
  getFeatured(id) {
    return (http.defaults.baseURL + "/static/img/featured/" + String(id));
  }
  getPlant(data) {
    return http.post("/getPlant", data);
  }
  getPlants(data) {
    return http.post("/getPlants", data);
  }
  getMyPlants(data) {
    return http.post("/getMyPlants", data);
  }
  addPlant(data) {
    return http.post("/addPlant", data);
  }
  addUserPlant(data) {
    return http.post("/addUserPlant", data);
  }
  getImagePaths(data) {
    return http.post("/getPlantImages", data);
  }
  uploadImage(file, onUploadProgress) {
    let formData = new FormData();
    formData.append("photo", file);
    return http.post("/uploadImage", formData, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      onUploadProgress
    });
  }
  getDisease(data) {
    return http.post("/getDisease", data);
  }
  getDiseases() {
    return http.post("/getDiseases");
  }
}

export default new Plants();