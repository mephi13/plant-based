import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import store from "./store";
import router from './router'
import { BootstrapVue, IconsPlugin, ToastPlugin } from 'bootstrap-vue'


//import plików stylu css z bootstrapa
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '../custombulma/css/mystyles.css';

Vue.config.productionTip = false

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.use(ToastPlugin)


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
