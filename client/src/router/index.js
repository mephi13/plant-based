import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Plants from '../views/Plants.vue'
import Login from '../views/Login.vue'
import Logout from "../views/Logout.vue"
import Register from "../views/Register.vue"
import Admin from "../views/Admin.vue"
import MyPlants from "../views/MyPlants.vue"

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/plants',
    name: `Plants`,
    component: Plants,
  },
  {
    path: '/myPlants',
    name: `My Plants`,
    component: MyPlants,
  },
  {
    path: '/login',
    name: `Login`,
    component: Login,
  }, 
  {
    path: '/register',
    name: `Register`,
    component: Register,
  }, 
  {
    path: '/logout',
    name: `Logout`,
    component: Logout,
  },
  {
    path: '/admin',
    name: `Admin`,
    component: Admin,
  },
  {
    path: '/adminPanel',
    name: `Admin Panel`,
    beforeEnter() {
      window.location.href = " http://plantbased.dev:5000/admin";
    }},
  {
    path: '/plant/:id',
    name: 'PlantSingle',
    component: () => import('../components/PlantSingle.vue'),
  },
  {
    path: '/newPlant',
    name: `New Plant`,
    component: () => import('../views/NewPlant.vue'),
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  linkExactActiveClass: 'is-hovered is-active',
})

export default router
