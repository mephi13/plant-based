import Vue from "vue";
import Vuex from "vuex";
 
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: {
        username: '',
        email: '',
        authenticated: false,
        authorized: false
      }
  },
  getters: {
      getUser: state => {
        var user = {
            username: state.user.username,
            email: state.user.username,
            authenticated: state.user.authenticated,
            authorized: state.user.authorized,
        }
        return user
      }
  },
  mutations: {
    changeUser (state, payload) {
        state.user.username = payload.username
        state.user.email = payload.email
        state.user.authenticated = payload.authenticated
        state.user.authorized = payload.authorized
      }
  },
  actions: {}
 });